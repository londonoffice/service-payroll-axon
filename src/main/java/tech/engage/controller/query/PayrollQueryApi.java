package tech.engage.controller.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.engage.query.client.Client;
import tech.engage.query.client.ClientRepository;
import tech.engage.query.components.Component;
import tech.engage.query.components.ComponentRepository;
import tech.engage.query.payroll.Payroll;
import tech.engage.query.payroll.PayrollRepository;

import java.util.List;

@RequestMapping("/query")
@RestController
public class PayrollQueryApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayrollQueryApi.class);

    @Autowired
    private PayrollRepository payrollRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ComponentRepository componentRepository;

    @GetMapping("clients")
    public List<Client> getClients() {

        LOGGER.info("> Query API: get clients");
        return clientRepository.findAll();
    }

    @GetMapping("payrolls")
    public List<Payroll> getPayrolls() {

        LOGGER.info("> Query API: get payrolls");
        return payrollRepository.findAll();
    }

    @GetMapping("components")
    public List<Component> getComponents() {

        LOGGER.info("> Query API: get components");
        return componentRepository.findAll();
    }

}