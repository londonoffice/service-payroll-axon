package tech.engage.controller.command;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
public class CreateClientDto {

    @NotBlank(message = "Client Legal Entity ID cannot be empty")
    String clientLegalEntityId;

    @NotBlank(message = "Name cannot be empty")
    String name;
}
