package tech.engage.controller.command;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
public class AddSystemComponentDto {

    @NotBlank(message = "Name cannot be empty")
    String name;
}
