package tech.engage.controller.command;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.commandhandling.model.AggregateNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tech.engage.command.*;
import tech.engage.domain.Frequency;
import tech.engage.domain.Scheme;

import javax.validation.Valid;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RequestMapping("/command")
@RestController
public class PayrollCommandApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayrollCommandApi.class);

    private final CommandGateway commandGateway;

    @Autowired
    public PayrollCommandApi(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping("clients")
    public CompletableFuture<String> createClient(@RequestBody @Valid CreateClientDto createClientDto) {
        LOGGER.info("> Command API: create client: " + createClientDto.toString());
        return commandGateway
                .send(new CreateClientCommand(createClientDto.getClientLegalEntityId(),
                        createClientDto.getName()));
    }

    @PostMapping("clients/{clientLegalEntityId}/payrolls")
    public CompletableFuture<String> createPayroll(@PathVariable String clientLegalEntityId,
            @RequestBody @Valid CreatePayrollDto createPayrollDto) {
        LOGGER.info("> Command API: create payroll: " + createPayrollDto.toString());
        String id =
                clientLegalEntityId + "-" + createPayrollDto.getName() + "-" + createPayrollDto.getScheme()
                        + "-" +
                        createPayrollDto.getFrequency();
        return commandGateway
                .send(new CreatePayrollCommand(id, clientLegalEntityId,
                        createPayrollDto.getName(),
                        Scheme.valueOf(createPayrollDto.getScheme()),
                        Frequency.valueOf(createPayrollDto.getFrequency())));
    }

    @DeleteMapping("clients/{clientLegalEntityId}/payrolls/{payrollId}")
    public CompletableFuture<String> closePayroll(@PathVariable String payrollId) {
        LOGGER.info("> Command API: close payroll: " + payrollId);
        return commandGateway.send(new ClosePayrollCommand(payrollId));
    }

    @PostMapping("components")
    public CompletableFuture<String> addSystemComponent(
            @RequestBody @Valid AddSystemComponentDto addSystemComponentDto) {
        LOGGER.info("> Command API: " + addSystemComponentDto.toString());
        String id = String.valueOf(UUID.randomUUID());
        return commandGateway
                .send(new AddSystemComponentCommand(id,
                        addSystemComponentDto.getName()));
    }

    @PostMapping("clients/{clientLegalEntityId}/components")
    public CompletableFuture<String> addCustomComponent(@PathVariable String clientLegalEntityId,
            @RequestBody @Valid AddSystemComponentDto addSystemComponentDto) {
        LOGGER.info("> Command API: " + addSystemComponentDto.toString());
        String id = String.valueOf(UUID.randomUUID());
        return commandGateway
                .send(new AddCustomComponentCommand(id,
                        clientLegalEntityId,
                        addSystemComponentDto.getName()));
    }

    @ExceptionHandler(AggregateNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void notFound() {
    }

}