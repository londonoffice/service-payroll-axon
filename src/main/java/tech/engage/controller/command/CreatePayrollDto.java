package tech.engage.controller.command;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import tech.engage.domain.Frequency;
import tech.engage.domain.Scheme;
import tech.engage.config.StringEnumeration;

@Data
@NoArgsConstructor
public class CreatePayrollDto {

    @NotBlank(message = "Name cannot be empty")
    String name;

    @NotBlank(message = "Scheme cannot be empty")
    @StringEnumeration(enumClass = Scheme.class, message = "The value ${validatedValue} is not a valid Scheme. Valid Schemes are: PAYE, CONTRACTOR")
    String scheme;

    @NotBlank(message = "Frequency cannot be empty")
    @StringEnumeration(enumClass = Frequency.class, message = "The value ${validatedValue} is not a valid Frequency. Valid Frequencies are WEEKLY, FORTNIGHTLY, MONTHLY, FOURWEEKLY")
    String frequency;
}
