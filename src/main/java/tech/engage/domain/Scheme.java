package tech.engage.domain;

public enum Scheme {
    PAYE,
    CONTRACTOR;
}
