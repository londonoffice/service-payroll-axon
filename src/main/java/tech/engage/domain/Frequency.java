package tech.engage.domain;

public enum Frequency {
    WEEKLY,
    FORTNIGHTLY,
    MONTHLY,
    FOURWEEKLY;
}
