package tech.engage;

import org.axonframework.config.EventHandlingConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableConfigurationProperties
public class ServicePayrollAxonApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServicePayrollAxonApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext =
                SpringApplication.run(ServicePayrollAxonApplication.class, args);
        logAccessURLs(applicationContext);
    }

    @Autowired
    public void configure(EventHandlingConfiguration config) {
        config.usingTrackingProcessors(); // default all processors to tracking mode.
    }

    private static void logAccessURLs(ConfigurableApplicationContext applicationContext) {
        ConfigurableEnvironment env = applicationContext.getEnvironment();
        String contextPath = env.getProperty("server.contextPath", "");
        String hostAddress = "unknown";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            // do nothing
        }
        LOGGER.info("Access URLs:\n----------------------------------------------------------\n\t" +
                        "Local: \t\thttp://localhost{}\n\t" +
                        "External: \thttp://{}{}\n\t" +
                        "Swagger: \thttp://localhost{}/swagger-ui.html" +
                        "\n----------------------------------------------------------",
                contextPath,
                hostAddress,
                contextPath,
                contextPath);
    }
}
