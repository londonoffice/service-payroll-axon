package tech.engage.command;

import lombok.Value;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import javax.validation.constraints.NotNull;

@Value
public class AddCustomComponentCommand {

    @TargetAggregateIdentifier
    @NotNull
    String componentId;

    @NotNull
    String clientLegalEntityId;

    @NotNull
    String name;

}