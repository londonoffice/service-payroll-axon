package tech.engage.command;

import lombok.Value;
import org.axonframework.commandhandling.TargetAggregateIdentifier;
import tech.engage.domain.Frequency;
import tech.engage.domain.Scheme;

import javax.validation.constraints.NotNull;

@Value
public class CreatePayrollCommand {

    @TargetAggregateIdentifier
    @NotNull
    String payrollId;

    @NotNull
    String legalEntityId;

    @NotNull
    String name;

    @NotNull
    Scheme scheme;

    @NotNull
    Frequency frequency;

}