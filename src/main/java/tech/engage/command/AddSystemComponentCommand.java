package tech.engage.command;

import lombok.Value;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import javax.validation.constraints.NotNull;

@Value
public class AddSystemComponentCommand {

    @TargetAggregateIdentifier
    @NotNull
    String componentId;

    @NotNull
    String name;

}