package tech.engage.command;

import lombok.Value;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import javax.validation.constraints.NotNull;

@Value
public class CreateClientCommand {

    @TargetAggregateIdentifier
    String clientLegalEntityId;

    @NotNull
    String name;

}