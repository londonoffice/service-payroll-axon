package tech.engage.aggregate;

import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.engage.command.AddCustomComponentCommand;
import tech.engage.command.AddSystemComponentCommand;
import tech.engage.event.CustomComponentAddedEvent;
import tech.engage.event.SystemComponentAddedEvent;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@Aggregate
public class Component implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Component.class);

    @AggregateIdentifier
    private String componentId;

    private String name;

    private String clientLegalEntityId;

    // Commands
    @CommandHandler
    public Component(AddSystemComponentCommand command) {
        LOGGER.info("> Command: add system component: " + command.toString());
        SystemComponentAddedEvent event = new SystemComponentAddedEvent(command.getComponentId(),
                command.getName(),
                LocalDateTime.now());
        LOGGER.info(">> Event: " + event.toString());
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public Component(AddCustomComponentCommand command) {
        LOGGER.info("> Command: add custom component: " + command.toString());
        CustomComponentAddedEvent event = new CustomComponentAddedEvent(command.getComponentId(),
                command.getClientLegalEntityId(),
                command.getName(),
                LocalDateTime.now());
        LOGGER.info(">> Event: " + event.toString());
        AggregateLifecycle.apply(event);
    }

    // Events -> Rehydrate state of the aggregate
    @EventSourcingHandler
    protected void on(SystemComponentAddedEvent event) {
        this.componentId = event.getComponentId();
        this.name = event.getName();
    }

    @EventSourcingHandler
    protected void on(CustomComponentAddedEvent event) {
        this.componentId = event.getComponentId();
        this.name = event.getName();
        this.clientLegalEntityId = event.getClientLegalEntityId();
    }
}
