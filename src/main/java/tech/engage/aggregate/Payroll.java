package tech.engage.aggregate;

import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import tech.engage.command.ClosePayrollCommand;
import tech.engage.command.CreatePayrollCommand;
import tech.engage.domain.Frequency;
import tech.engage.domain.Scheme;
import tech.engage.event.PayrollClosedEvent;
import tech.engage.event.PayrollCreatedEvent;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@Aggregate
public class Payroll implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Payroll.class);

    @AggregateIdentifier
    private String payrollId;

    private String clientLegalEntityId;

    private String name;

    private Scheme scheme;

    private Frequency frequency;

    // Commands
    @CommandHandler
    public Payroll(CreatePayrollCommand command) {
        LOGGER.info("> Command: create payroll: " + command.toString());
        PayrollCreatedEvent event = new PayrollCreatedEvent(command.getPayrollId(),
                command.getLegalEntityId(),
                command.getName(),
                command.getScheme(),
                command.getFrequency(),
                LocalDateTime.now()
        );
        LOGGER.info(">> Event: " + event.toString());
        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    protected void on(ClosePayrollCommand command) {
        LOGGER.info("> Command: close payroll: " + command.toString());
        PayrollClosedEvent event = new PayrollClosedEvent(command.getPayrollId(), LocalDateTime.now());
        LOGGER.info(">> Event: " + event.toString());
        AggregateLifecycle.apply(event);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public static class PayrollCreationFailedException extends RuntimeException {
        PayrollCreationFailedException(String message) {
            super(message);
        }

    }

    // Events -> Rehydrate state of the aggregate
    @EventSourcingHandler
    protected void on(PayrollCreatedEvent event) {
        this.payrollId = event.getPayrollId();
        this.clientLegalEntityId = event.getClientLegalEntityId();
        this.name = event.getName();
        this.scheme = event.getScheme();
        this.frequency = event.getFrequency();
    }

    @EventSourcingHandler
    protected void on(PayrollClosedEvent event) {
        AggregateLifecycle.markDeleted();
    }
}
