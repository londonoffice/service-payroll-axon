package tech.engage.aggregate;

import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.engage.command.CreateClientCommand;
import tech.engage.event.ClientCreatedEvent;
import tech.engage.event.CustomComponentAddedEvent;

import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@Aggregate
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Client.class);

    @AggregateIdentifier
    private String clientLegalEntityId;

    private String name;

    // Commands
    @CommandHandler
    public Client(CreateClientCommand command) {
        LOGGER.info("> Command: create client: " + command.toString());
        ClientCreatedEvent event = new ClientCreatedEvent(command.getClientLegalEntityId(),
                command.getName(),
                LocalDateTime.now());
        LOGGER.info(">> Event: " + event.toString());
        AggregateLifecycle.apply(event);
    }

    // Events -> Rehydrate state of the aggregate
    @EventSourcingHandler
    protected void on(ClientCreatedEvent event) {
        this.clientLegalEntityId = event.getClientlegalEntityId();
        this.name = event.getName();
    }
}
