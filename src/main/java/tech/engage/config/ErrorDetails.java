package tech.engage.config;

import lombok.Value;

@Value
public class ErrorDetails {
    private String timestamp;

    private String message;
}