package tech.engage.query.payroll;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
public class Payroll {

    @Id
    @GeneratedValue
    private Long id;

    private String payrollId;

    private String legalEntityId;

    private String name;

    private String scheme;

    private String frequency;

    private Boolean active;

    private String createdAt;

    private String closedAt;

}
