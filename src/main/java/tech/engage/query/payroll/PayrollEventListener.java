package tech.engage.query.payroll;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.engage.event.PayrollClosedEvent;
import tech.engage.event.PayrollCreatedEvent;

@Component
@ProcessingGroup("engage-tracking")
public class PayrollEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(PayrollEventListener.class);

    private PayrollRepository repository;

    @Autowired
    public PayrollEventListener(PayrollRepository repository) {
        this.repository = repository;
    }

    @EventHandler
    public void on(PayrollCreatedEvent event) {
        Payroll payroll = new Payroll();
        payroll.setPayrollId((event.getPayrollId()));
        payroll.setLegalEntityId(event.getClientLegalEntityId());
        payroll.setName(event.getName());
        payroll.setScheme(event.getScheme().toString());
        payroll.setFrequency(event.getFrequency().toString());
        payroll.setActive(true);
        payroll.setCreatedAt(event.getCreatedAt().toString());
        LOGGER.info(">>> Query: creating payroll " + event.getPayrollId());
        repository.save(payroll);
    }

    @EventHandler
    public void on(PayrollClosedEvent event) {
        Payroll payroll = repository.findDistinctByPayrollId(event.getPayrollId());
        payroll.setActive(false);
        payroll.setClosedAt(event.getClosedAt().toString());

        LOGGER.info(">>> Query: closing payroll " + event.getPayrollId());
        repository.save(payroll);
    }
}