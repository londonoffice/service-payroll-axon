package tech.engage.query.payroll;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PayrollRepository extends JpaRepository<Payroll, String> {

    Payroll findDistinctByPayrollId(String payrollId);
}
