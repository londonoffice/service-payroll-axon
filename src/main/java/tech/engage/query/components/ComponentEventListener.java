package tech.engage.query.components;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tech.engage.event.CustomComponentAddedEvent;
import tech.engage.event.SystemComponentAddedEvent;
import tech.engage.query.client.ClientRepository;

@org.springframework.stereotype.Component
@ProcessingGroup("engage-tracking")
public class ComponentEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComponentEventListener.class);

    private ComponentRepository componentRepository;

    private ClientRepository clientRepository;

    @Autowired
    public ComponentEventListener(ComponentRepository componentRepository, ClientRepository clientRepository) {

        this.componentRepository = componentRepository;
        this.clientRepository = clientRepository;
    }

    @EventHandler
    public void on(CustomComponentAddedEvent event) {

        Component component = new Component();
        component.setClientLegalEntityId(event.getClientLegalEntityId());
        component.setName(event.getName());
        component.setSystemComponent(false);
        component.setAddedAt(event.getAddedAt().toString());
        LOGGER.info(">>> Query: adding custom component: " + component);
        componentRepository.save(component);
    }

    @EventHandler
    public void on(SystemComponentAddedEvent event) {

        clientRepository.findAll().forEach(client -> {
                    Component component = new Component();
                    component.setClientLegalEntityId(client.getLegalEntityId());
                    component.setName(event.getName());
                    component.setSystemComponent(true);
                    component.setAddedAt(event.getAddedAt().toString());
                    LOGGER.info(">>> Query: adding system component: " + component);
                    componentRepository.save(component);
                }
        );
    }
}