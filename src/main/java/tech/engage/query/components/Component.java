package tech.engage.query.components;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Component {

    @Id
    @GeneratedValue
    private Long id;

    private String clientLegalEntityId;

    private String name;

    private boolean systemComponent;

    private String addedAt;

}
