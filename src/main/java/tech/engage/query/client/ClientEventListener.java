package tech.engage.query.client;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.engage.event.ClientCreatedEvent;

@Component
@ProcessingGroup("engage-tracking")
public class ClientEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientEventListener.class);

    private ClientRepository repository;

    @Autowired
    public ClientEventListener(ClientRepository repository) {
        this.repository = repository;
    }

    @EventHandler
    public void on(ClientCreatedEvent event) {
        Client client = new Client();
        client.setLegalEntityId(event.getClientlegalEntityId());
        client.setName(event.getName());
        client.setCreatedAt(event.getCreatedAt().toString());
        LOGGER.info(">>> Query: creating client " + event.getClientlegalEntityId());
        repository.save(client);
    }
}