package tech.engage.event;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class ClientCreatedEvent {

    String clientlegalEntityId;

    String name;

    LocalDateTime createdAt;
}
