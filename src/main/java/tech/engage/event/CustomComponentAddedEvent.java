package tech.engage.event;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class CustomComponentAddedEvent {
    String componentId;

    String clientLegalEntityId;

    String name;

    LocalDateTime addedAt;
}
