package tech.engage.event;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class SystemComponentAddedEvent {
    String componentId;

    String name;

    LocalDateTime addedAt;
}
