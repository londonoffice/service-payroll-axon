package tech.engage.event;

import lombok.Value;
import tech.engage.domain.Frequency;
import tech.engage.domain.Scheme;

import java.time.LocalDateTime;

@Value
public class PayrollCreatedEvent {
    String payrollId;

    String clientLegalEntityId;

    String name;

    Scheme scheme;

    Frequency frequency;

    LocalDateTime createdAt;
}
