package tech.engage.event;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class PayrollClosedEvent {
    String payrollId;

    LocalDateTime closedAt;
}
