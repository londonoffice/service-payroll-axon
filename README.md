# Payroll Axon

## Introduction

This is a sample application to demonstrate Payroll CQRS/EventSourcing.

## Building

> mvn clean package

## TODO
Replaying events
1) Stop the service
2) Delete from token_entry
3) Delete from query table e.g. payroll
4) Start the service

## TODO
- What are the MVC endpoints?
- Authentication?
- How to view components against clients?
- How to view period sets
- How to view supplier/client/provider

## Documentation

* Axon Framework - http://www.axonframework.org/
* Spring Boot - http://projects.spring.io/spring-boot/
* Spring Framework - http://projects.spring.io/spring-framework/
* Spring Data JPA - https://projects.spring.io/spring-data-jpa/# Spring Boot Axon Sample

## Scratchpad

http POST http://localhost:9000/pay clientId=123 name=myPayroll scheme=PAYE frequency=WEEKLY
http POST http://localhost:9001/pay/command clientId=22326 name=myPayroll scheme=PAYE frequency=WEEKLY
http DELETE http://localhost:9001/pay/command/642a1c20-d804-4ffd-a390-d36934faf7a2
http http://localhost:9001/pay/query
docker-compose up --scale payroll-axon=2                                               